
### bahnDirect – What this app was about?

In 2020/2021, I had the idea of creating a small app that serves only the purpose of loading current tickets (including BahnCard discount cards, which - at least in the legacy system – are treated much similar to tickets). Thus, I created this app that allowed me to hardcode my credentials and EBC (electronic BahnCard) number, and those would be shown in a small fancy layout.

I offer a [demonstration video of what the UI looked like](bahncard.mp4), purely to satisfy any curiosities.

It would presumably be much cleaner and more fun to rebuild this application with Next Navigator API. On the other hand, this is not allowed by the terms of use; I am inclined to recommend against building apps for services whose provider is not in favor of you building your app.

**Recommended alternative**: [NVM and its members](https://deutschlandticket-mainfranken.de) offer the 49eur (Deutschlandticket) in `.pkpass` format, which can be used with [PassAndroid](https://github.com/ligi/PassAndroid/) or Apple Wallet.

_That's how you travel today._

---

**The information below this line is entirely historical and not applicable to Next DB.**

---

##### How to find out your EBC (electronic BahnCard number) – suggestion. Other and more efficient ways are possible

1. Create bahn.de account
2. Attach your BahnCard by signing up for BahnCard services
3. Root your phone
4. Install DB Navigator
5. Sign in to your account
6. Download BahnCard in DB Navigator
7. Do the following commands on your computer after attaching your phone:

```
$ adb root
restarting adbd as root
$ adb shell
:/# sqlite3 /data/data/de.hafas.android.db/databases/DBWallet.db
SQLite version 3.22.0 2019-09-03 18:36:11
Enter ".help" for usage hints.
sqlite> .tables
DBC_BAHNCARD           DBC_ORDERS             DBC_SECONDARY_TICKETS
DBC_BARCODES           DBC_ORDER_LABELS       DBC_SECTIONS         
DBC_BC_SELFSERVICES    DBC_POSITIONS          DBC_STOPS            
DBC_FAVOURITES         DBC_RESERVATIONS       DBC_TICKETS          
DBC_HVV_TICKETS        DBC_SCHEDULES          DBC_VERBUNDFAV       
DBC_MAIN_TICKETS       DBC_SCI_STATUS         android_metadata     
DBC_MOREAPPS           DBC_SEAT             
sqlite> SELECT * FROM DBC_TICKETS;
```

The last command will list all tickets including BahnCards that are stored in your DB Navigator installation.

##### How to install

At the top of `TicketCollector`, look for

```
private static final String EBC = "";
private static final String AUTH_USERNAME = "";
private static final String AUTH_PASSWORD = "":
```

and fill in your details.
