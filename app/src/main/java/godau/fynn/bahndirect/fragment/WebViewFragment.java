package godau.fynn.bahndirect.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import godau.fynn.bahndirect.R;
import godau.fynn.bahndirect.model.BahnCard;

public class WebViewFragment extends Fragment {

    private static final String ARG_BAHNCARD = "BahnCard";

    public WebViewFragment() {}

    public static WebViewFragment get(BahnCard bahnCard) {
        WebViewFragment fragment = new WebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_BAHNCARD, bahnCard);
        fragment.setArguments(bundle);
        return fragment;
    }

    private BahnCard bahnCard;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_webview, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bahnCard = (BahnCard) getArguments().getSerializable(ARG_BAHNCARD);

        final WebView webView = view.findViewById(R.id.webview);

        webView.loadData(wrap(bahnCard.svg), "text/html;charset=utf-8", "UTF-8");

        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);

    }

    private String wrap(String svg) {
        return "<center>" + svg + "</center>";
    }
}
