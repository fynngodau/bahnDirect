package godau.fynn.bahndirect.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import godau.fynn.bahndirect.Base64Util;
import godau.fynn.bahndirect.R;
import godau.fynn.bahndirect.model.BahnCard;

import java.text.SimpleDateFormat;

public class BahnCardFragment extends Fragment {

    private static final String ARG_BAHNCARD = "BahnCard";

    public BahnCardFragment() {}

    public static BahnCardFragment get(BahnCard bahnCard) {
        BahnCardFragment fragment = new BahnCardFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_BAHNCARD, bahnCard);
        fragment.setArguments(bundle);
        return fragment;
    }

    private BahnCard bahnCard;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_bahncard, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bahnCard = (BahnCard) getArguments().getSerializable(ARG_BAHNCARD);

        final TextView cardTypeView = view.findViewById(R.id.cardType);
        final TextView cardOwnerView = view.findViewById(R.id.cardOwner);
        final TextView cardNumberView = view.findViewById(R.id.cardNumber);
        final TextView textView = view.findViewById(R.id.text);
        final TextView validUntilTextView = view.findViewById(R.id.validUntil);

        final ImageView atztecView = view.findViewById(R.id.atztec);
        final ImageView sichtmerkmalView = view.findViewById(R.id.sichtmerkmal);

        textView.setText(bahnCard.toString());

        cardTypeView.setText(bahnCard.cardName);
        cardOwnerView.setText(bahnCard.owner);
        cardNumberView.setText(bahnCard.cardNumber);

        atztecView.setImageBitmap(Base64Util.convertStringToBitmap(bahnCard.atztec));
        sichtmerkmalView.setImageBitmap(Base64Util.convertStringToBitmap(bahnCard.sichtmerkmal));


        SimpleDateFormat format = new SimpleDateFormat("MM\nyy");
        validUntilTextView.setText(format.format(bahnCard.cardValidityEnd));
    }
}
