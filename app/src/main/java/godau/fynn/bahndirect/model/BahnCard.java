package godau.fynn.bahndirect.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;
import godau.fynn.bahndirect.persistence.Converters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Entity(tableName = "bahnCard")
public class BahnCard implements Serializable {

    public int cardClass, cardDiscount;

    @PrimaryKey @NonNull
    public String ebc;

    public String owner, cardName, cardNumber,
            atztec, sichtmerkmal, svg;

    @TypeConverters(Converters.class)
    public Date aztecValidityBegin, aztecValidityEnd, cardValidityBegin, cardValidityEnd;

    private static final SimpleDateFormat format = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss", Locale.GERMANY);

    /**
     * (Almost) empty constructor for Room
     */
    public BahnCard(String ebc) {
        this.ebc = ebc;
    }

    public BahnCard(Document queryDocument) {

        Node nvplist = queryDocument.getElementsByTagName("nvplist").item(0);

        NodeList nodeList = nvplist.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node n = nodeList.item(i);

            String text = n.getTextContent();

            switch (n.getAttributes().getNamedItem("name").getTextContent()) {
                case "klasse":
                    cardClass = Integer.parseInt(text);
                    break;
                case "inhaber":
                    owner = text;
                    break;
                case "produkt":
                    cardName = text;
                    break;
                case "bcnummer":
                    cardNumber = text;
                    break;
                case "ebcbarcodegueltigab":
                    try {
                        aztecValidityBegin = format.parse(text);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                case "ebcbarcodegueltigbis":
                    try {
                        aztecValidityEnd = format.parse(text);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                case "rbs":
                    cardDiscount = Integer.parseInt(text);
            }

        }

        Node order = queryDocument.getElementsByTagName("order").item(0);

        ebc = order.getAttributes().getNamedItem("on").getTextContent();

        try {
            cardValidityBegin = format.parse(order.getAttributes().getNamedItem("vfrom").getTextContent());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            cardValidityEnd = format.parse(order.getAttributes().getNamedItem("vto").getTextContent());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        NodeList hts = queryDocument.getElementsByTagName("ht");
        for (int i = 0; i < hts.getLength(); i++) {
            Node ht = hts.item(i);
            String name = ht.getAttributes().getNamedItem("name").getNodeValue();

            switch (name) {
                case "barcode":
                    atztec = ht.getTextContent().replaceAll("data:image/png;base64,\n", "");
                    continue;

                case "sichtmerkmal":
                    sichtmerkmal = ht.getTextContent()
                            .replaceAll("data:image/png;base64,", "");

                    continue;
            }
        }

        svg = queryDocument.getElementsByTagName("tcklist").item(0).getFirstChild().getLastChild().getTextContent();


    }

    @NonNull
    @Override
    public String toString() {

        SimpleDateFormat stringFormat = new SimpleDateFormat("dd.MM.yy");

        return "This " + cardName + " provides a " + cardDiscount + "% discount for "
                + cardClass + (cardClass == 1? "st" : "nd") + " class to " + owner + "."
                + "\n\n" + "It is valid from " + stringFormat.format(cardValidityBegin) + " until "
                + stringFormat.format(cardValidityEnd) + "."
                + "\n\n" + "Its order number is " + ebc + ". This Atztec code is valid until "
                + stringFormat.format(aztecValidityEnd) + ".";
    }
}
