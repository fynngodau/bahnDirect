package godau.fynn.bahndirect.download;

import android.content.Context;
import godau.fynn.bahndirect.Network;
import godau.fynn.bahndirect.model.BahnCard;
import godau.fynn.bahndirect.persistence.TicketDatabase;

@Deprecated // TODO replace with bahnDirect#2
public class TicketCollector {

    private static final String EBC = "";
    private static final String AUTH_USERNAME = "";
    private static final String AUTH_PASSWORD = "";


    public static BahnCard collectBahnCard(Context context) {
        BahnCard bahnCard = new Downloader(new Network(context)).onlineDownload(EBC, AUTH_USERNAME, AUTH_PASSWORD);

        if (bahnCard != null) {

            TicketDatabase database = TicketDatabase.get(context);
            database.getTicketDao().insert(bahnCard);
            database.close();

            return bahnCard;
        } else {
            return getBahnCardFromDatabase(context);
        }
    }

    private static BahnCard getBahnCardFromDatabase(Context context) {
        TicketDatabase database = TicketDatabase.get(context);
        BahnCard[] bahnCards = database.getTicketDao().getBahnCards();
        database.close();
        if (bahnCards.length == 0) return null;
        else return bahnCards[0];
    }
}
