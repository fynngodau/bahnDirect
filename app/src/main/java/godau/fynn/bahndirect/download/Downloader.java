package godau.fynn.bahndirect.download;

import android.util.Log;

import godau.fynn.bahndirect.Network;
import godau.fynn.bahndirect.model.BahnCard;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.charset.Charset;

public class Downloader {

    private static final String ENDPOINT = "https://fahrkarten.bahn.de/mobile/dbc/xs.go?";

    private final Network network;

    public Downloader(Network network) {
        this.network = network;
    }

    public BahnCard onlineDownload(String ebc, String username, String password) {

        try {
            InputStream stream = network.request(ENDPOINT,
                    "<?xml version=\"1.0\"?><rqorderdetails version=\"1.0\"><rqheader tnr=\"C8BB9A35-D663-4DE7-ACA1-41FD6260\" ts=\"2021-07-02T15:45:19\" l=\"de\" v=\"20040000\" d=\"iPhone8,1\" os=\"iOS_13.4.1\" app=\"NAVIGATOR\"/><rqorder on=\"" + ebc + "\"/><authlogin user=\"" + username + "\" pw=\"" + password + "\"><sso genToken=\"FALSE\"/></authlogin></rqorderdetails>", "POST");


            final StringBuilder textBuilder = new StringBuilder();

            Reader reader = new BufferedReader(new InputStreamReader
                    (stream, Charset.forName("UTF-8")));
            int c;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
            reader.close();

            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse(new ByteArrayInputStream(textBuilder.toString().getBytes()));

            Log.d(Downloader.class.getSimpleName(), textBuilder.toString());
            return new BahnCard(document);
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }
}
