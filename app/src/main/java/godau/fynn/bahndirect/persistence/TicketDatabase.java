package godau.fynn.bahndirect.persistence;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import godau.fynn.bahndirect.model.BahnCard;

@Database(
        entities = BahnCard.class,
        version = 1
)
public abstract class TicketDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "tickets";

    public static TicketDatabase get(Context context) {
        return Room.databaseBuilder(context, TicketDatabase.class, DATABASE_NAME)
                .addMigrations(
                )
                .build();

    }

    public abstract TicketDao getTicketDao();


}
