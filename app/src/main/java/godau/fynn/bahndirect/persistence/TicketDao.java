package godau.fynn.bahndirect.persistence;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import godau.fynn.bahndirect.model.BahnCard;

@Dao
public abstract class TicketDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(BahnCard bahnCard);

    @Query("SELECT * FROM bahnCard")
    public abstract BahnCard[] getBahnCards();
}
