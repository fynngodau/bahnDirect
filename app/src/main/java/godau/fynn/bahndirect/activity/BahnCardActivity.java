package godau.fynn.bahndirect.activity;

import android.os.Bundle;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import godau.fynn.bahndirect.BahnCardViewPagerAdapter;
import godau.fynn.bahndirect.R;
import godau.fynn.bahndirect.download.TicketCollector;
import godau.fynn.bahndirect.model.BahnCard;

public class BahnCardActivity extends FragmentActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bahncard);

        final ViewPager2 viewPager = findViewById(R.id.viewPager);

        new Thread(() -> {

            BahnCard bahnCard = TicketCollector.collectBahnCard(BahnCardActivity.this);

            runOnUiThread(() -> {

                if (bahnCard == null) {
                    Toast.makeText(this, "Offline restore failed, please establish network connection", Toast.LENGTH_SHORT).show();
                    return;
                }

                viewPager.setOffscreenPageLimit(2);
                viewPager.setAdapter(new BahnCardViewPagerAdapter(BahnCardActivity.this, bahnCard));
            });

        }).start();
    }


}
