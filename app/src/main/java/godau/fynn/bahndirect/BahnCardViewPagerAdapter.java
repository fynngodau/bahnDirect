package godau.fynn.bahndirect;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import godau.fynn.bahndirect.fragment.BahnCardFragment;
import godau.fynn.bahndirect.fragment.WebViewFragment;
import godau.fynn.bahndirect.model.BahnCard;

public class BahnCardViewPagerAdapter extends FragmentStateAdapter {

    private final BahnCard bahnCard;

    public BahnCardViewPagerAdapter(@NonNull FragmentActivity fragmentActivity, BahnCard bahnCard) {
        super(fragmentActivity);
        this.bahnCard = bahnCard;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position == 0) {
            return BahnCardFragment.get(bahnCard);
        } else if (position == 1) {
            return WebViewFragment.get(bahnCard);
        } else return null;
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
